# This file is part of photo21
# Copyright (C) 2022  Amicale des élèves de l'ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from allauth.socialaccount.providers.oauth2.urls import default_urlpatterns

from .provider import NoteKfetProvider

urlpatterns = default_urlpatterns(NoteKfetProvider)
