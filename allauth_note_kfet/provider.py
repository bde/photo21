# From https://gitlab.crans.org/bde/allauth-note-kfet
# Copyright (C) 2022  Amicale des élèves de l'ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from allauth.account.models import EmailAddress
from allauth.socialaccount.providers.base import ProviderAccount
from allauth.socialaccount.providers.oauth2.provider import OAuth2Provider


class NoteKfetAccount(ProviderAccount):
    def to_str(self):
        return self.account.extra_data.get("username")


class NoteKfetProvider(OAuth2Provider):
    id = "notekfet"
    name = "Note Kfet"
    account_class = NoteKfetAccount

    def extract_uid(self, data):
        return str(data["username"])

    def extract_common_fields(self, data):
        return dict(
            email=data.get("email"),
            username=data.get("username"),
            last_name=data.get("last_name"),
            first_name=data.get("first_name"),
        )

    def get_default_scope(self):
        return ["read"]

    def extract_email_addresses(self, data):
        ret = []
        email = data.get("email")
        if email:
            ret.append(EmailAddress(email=email, verified=True, primary=True))
        return ret


provider_classes = [NoteKfetProvider]
