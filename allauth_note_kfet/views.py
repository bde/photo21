# From https://gitlab.crans.org/bde/allauth-note-kfet
# Copyright (C) 2022  Amicale des élèves de l'ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

import requests
from allauth.socialaccount import app_settings
from allauth.socialaccount.providers.oauth2.views import (
    OAuth2Adapter,
    OAuth2CallbackView,
    OAuth2LoginView,
)

from .provider import NoteKfetProvider


class NoteKfetOAuth2Adapter(OAuth2Adapter):
    provider_id = NoteKfetProvider.id

    def complete_login(self, request, app, token, **kwargs):
        headers = {
            "Authorization": f"Bearer {token.token}",
            "Content-Type": "application/json",
        }
        extra_data = requests.get(self.profile_url, headers=headers)

        return self.get_provider().sociallogin_from_response(request, extra_data.json())

    @property
    def settings(self):
        return app_settings.PROVIDERS.get(self.provider_id, {})

    @property
    def domain(self):
        return self.settings.get("DOMAIN", "note.crans.org")

    @property
    def access_token_url(self):
        return f"https://{self.domain}/o/token/"

    @property
    def authorize_url(self):
        return f"https://{self.domain}/o/authorize/"

    @property
    def profile_url(self):
        return f"https://{self.domain}/api/me/"


oauth2_login = OAuth2LoginView.adapter_view(NoteKfetOAuth2Adapter)
oauth2_callback = OAuth2CallbackView.adapter_view(NoteKfetOAuth2Adapter)
