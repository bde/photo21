# Photo server 2021-2023

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0.txt)

This is the source code for the webserver hosting pictures from the
ENS Paris-Saclay student life.

The philosophy of this project is to keep this code as simple as possible to
run and to maintain.

## Setup

1.  **Dependency installation.**
    If you are not using Debian, please feel free to adapt the following instructions.

    ```bash
    sudo apt install git gettext python3-django python3-django-allauth python3-django-crispy-forms python3-docutils python3-exifread python3-pil

    # Only for production
    sudo apt install nginx uwsgi uwsgi-plugin-python3 python3-certbot-nginx
    ```

2.  **Cloning.**
    Change directory to where you want the project to be.
    In production, we usually use `/var/www/photos/` as the `root` user.

    ```bash
    git clone https://gitlab.crans.org/bde/photo21.git && cd photo21
    ```

3.  **Configuration (production only).**

    ```bash
    # Only for production
    sudo mkdir static media
    sudo chown www-data:www-data -R static media
    sudo chmod g+rwx -R static media
    sudo cp docs/uwsgi_photos.ini /etc/uwsgi/apps-available/uwsgi_photos.ini
    sudo ln -s /etc/uwsgi/apps-available/uwsgi_photos.ini /etc/uwsgi/apps-enabled/
    sudo cp docs/nginx_photos /etc/nginx/sites-available/photos.crans.org
    sudo ln -s /etc/nginx/sites-available/photos.crans.org /etc/nginx/sites-enabled/
    sudo cp docs/letsencrypt_photos.crans.org /etc/letsencrypt/conf.d/photos.crans.org
    sudo cp docs/renewal-hooks_post_nginx /etc/letsencrypt/renewal-hooks/post/nginx
    sudo certbot --config /etc/letsencrypt/conf.d/photos.crans.org.ini certonly
    ```

4.  **Database (production only).**
    In development, you may use SQLite (no setup).
    In production, we use PostgreSQL which require a bit of setup:

    ```bash
    sudo apt install postgresql postgresql-contrib
    sudo -u postgres psql
    postgres=# CREATE USER photo21 WITH PASSWORD 'un_mot_de_passe_sur';
    postgres=# CREATE DATABASE photo21 OWNER photo21;
    ```

5.  **Initialization.**,
    In production, please use `www-data` user.

    ```
    ./manage.py collectstatic
    ./manage.py check
    ./manage.py migrate
    ./manage.py compilemessages

    # Only when creating a new database
    ./manage.py loaddata initial
    ./manage.py createsuperuser
    # change DEBUG to True in photo21/settings.py
    ```

6.  *Enjoy \o/*

    In production, the NGINX site should now work.
    In development, you can launch the development server using:

    ```bash
    (env)$ ./manage.py runserver
    ```
