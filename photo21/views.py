# This file is part of photo21
# Copyright (C) 2021-2022  Amicale des élèves de l'ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.views.generic import ListView, View
from photologue.models import Gallery


class MediaAccess(LoginRequiredMixin, View):
    def get(self, request, path):
        response = HttpResponse()
        # Content-type will be detected by nginx
        del response["Content-Type"]
        response["X-Accel-Redirect"] = "/protected/media/" + path
        return response


class IndexView(LoginRequiredMixin, ListView):
    queryset = Gallery.objects.all()
    paginate_by = 4
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Get superusers
        user_model = get_user_model()
        superusers = user_model.objects.filter(is_superuser=True)
        context["superusers"] = superusers

        return context
