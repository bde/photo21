// This file is part of photo21
// Copyright (C) 2022  Amicale des élèves de l'ENS Paris-Saclay
// SPDX-License-Identifier: GPL-3.0-or-later

// On language selection, submit form
document.getElementsByName("language")[0].addEventListener("change", (e) => {
    e.target.form.submit();
});
