# This file is part of photo21
# Copyright (C) 2022  Amicale des élèves de l'ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

"""photo21 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
"""

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path, re_path

from .views import IndexView, MediaAccess

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("", include("photologue.urls", namespace="photologue")),
    path("accounts/", include("allauth.urls")),
    path("i18n/", include("django.conf.urls.i18n")),
    path("admin/doc/", include("django.contrib.admindocs.urls")),
    path("admin/", admin.site.urls),
]

# In production media are served through NGINX with X-Accel-Redirect
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
else:
    urlpatterns.append(
        re_path("^media/(?P<path>.*)", MediaAccess.as_view(), name="media")
    )
