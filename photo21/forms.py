# This file is part of photo21
# Copyright (C) 2022  Amicale des élèves de l'ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from allauth.account.forms import SignupForm
from django import forms
from django.utils.translation import gettext_lazy as _


class CustomSignupForm(SignupForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Add description on email field
        self.fields["email"].help_text = _(
            "Please enter a valid email address ending with `@crans.org` or "
            "`@ens-paris-saclay.fr`."
        )

    def clean_email(self):
        """
        Check that the email address ends with a trusted domain.
        """
        email = super().clean_email()
        if not email.endswith("@crans.org") and not email.endswith(
            "@ens-paris-saclay.fr"
        ):
            raise forms.ValidationError(
                _("Must end with `@crans.org` or `@ens-paris-saclay.fr`.")
            )
        return email
