# This file is part of photo21
# Copyright (C) 2022  Amicale des élèves de l'ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from django.apps import AppConfig


class PhotologueConfig(AppConfig):
    default_auto_field = "django.db.models.AutoField"
    name = "photologue"
