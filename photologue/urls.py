# This file is part of photo21
# Copyright (C) 2022  Amicale des élèves de l'ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from django.urls import path, re_path

from .views import (
    GalleryArchiveIndexView,
    GalleryDetailView,
    GalleryDownload,
    GalleryUpload,
    GalleryYearArchiveView,
    PhotoDeleteView,
    PhotoDetailView,
    PhotoReportView,
    TagDetail,
)

app_name = "photologue"
urlpatterns = [
    path("tag/<slug:slug>/", TagDetail.as_view(), name="tag-detail"),
    path("gallery/", GalleryArchiveIndexView.as_view(), name="pl-gallery-archive"),
    re_path(
        r"^gallery/(?P<year>\d{4})/$",
        GalleryYearArchiveView.as_view(),
        name="pl-gallery-archive-year",
    ),
    path("gallery/<slug:slug>/", GalleryDetailView.as_view(), name="pl-gallery"),
    path(
        "gallery/<slug:slug>/<int:owner>/",
        GalleryDetailView.as_view(),
        name="pl-gallery-owner",
    ),
    path(
        "gallery/<slug:slug>/download/",
        GalleryDownload.as_view(),
        name="pl-gallery-download",
    ),
    path("photo/<int:pk>/", PhotoDetailView.as_view(), name="pl-photo"),
    path("photo/<int:pk>/delete/", PhotoDeleteView.as_view(), name="pl-photo-delete"),
    path("photo/<int:pk>/report/", PhotoReportView.as_view(), name="pl-photo-report"),
    path("upload/", GalleryUpload.as_view(), name="pl-gallery-upload"),
]
