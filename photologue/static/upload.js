// This file is part of photo21
// Copyright (C) 2022  Amicale des élèves de l'ENS Paris-Saclay
// SPDX-License-Identifier: GPL-3.0-or-later

// When user drags files, register them in the file field
const dropZone = document.getElementById('drop_zone');
const uploadInput = document.getElementById('id_file_field');

dropZone.ondrop = function(e) {
    e.preventDefault();
    this.className = 'upload-drop-zone';
    console.log(e.dataTransfer.files)
    uploadInput.files = e.dataTransfer.files;
}

dropZone.ondragover = function() {
    this.className = 'upload-drop-zone drop';
    return false;
}

dropZone.ondragleave = function() {
    this.className = 'upload-drop-zone';
    return false;
}

// When user selects an existing gallery, disable new gallery fields
const gallerySelect = document.getElementById('id_gallery')
gallerySelectUpdate = () => {
    const useGallery = (gallerySelect.value !== "");
    document.getElementById('id_new_gallery_title').disabled = useGallery;
    document.getElementById('id_new_gallery_date_start').disabled = useGallery;
    document.getElementById('id_new_gallery_date_end').disabled = useGallery;
    document.getElementById('id_new_gallery_tags').disabled = useGallery;
}
gallerySelect.addEventListener('change', gallerySelectUpdate);
gallerySelectUpdate();

// On submit, show a message to make user wait
document.getElementById('upload_form').addEventListener('submit', (e) => {
    document.getElementById('submit-id-submit').disabled = true;
    document.getElementById('submit-id-submit').value = "Please be patient";
})
