// This file is part of photo21
// Copyright (C) 2022  Amicale des élèves de l'ENS Paris-Saclay
// SPDX-License-Identifier: GPL-3.0-or-later

// Init gallery
lightGallery(document.getElementById('lightgallery'), {
    plugins: [lgAdmin, lgHash, lgThumbnail, lgZoom],
    customSlideName: true,
    licenseKey: '94ED9732-30284A12-B88B0137-4FF9CEE6',
});
