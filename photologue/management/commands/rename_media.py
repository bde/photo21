# This file is part of photo21
# Copyright (C) 2022  Amicale des élèves de l'ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

import os
from pathlib import Path

from django.conf import settings
from django.core.management.base import BaseCommand
from photologue.models import Gallery


class Command(BaseCommand):
    help = "Rename uploaded media file to match gallery and photo names"

    def add_arguments(self, parser):
        parser.add_argument("--apply", action="store_true")

    def handle(self, *args, **options):
        media_dir = Path(settings.MEDIA_ROOT)
        for gallery in Gallery.objects.all():
            # Create gallery directory
            gallery_year = str(gallery.date_start.year)
            gallery_dir = Path("photos") / gallery_year / gallery.slug
            gallery_path = media_dir / gallery_dir
            if not gallery_path.exists():
                self.stdout.write(f"Creating {gallery_dir}")
                if options["apply"]:
                    gallery_path.mkdir(parents=True)

            # Move photos in gallery folder
            for photo in gallery.photos.all():
                photo_name = str(gallery_dir / photo.image.name.split("/")[-1])
                if photo.image.name == photo_name:
                    continue
                self.stdout.write(f"  Moving {photo.image.name} -> {photo_name}")
                if options["apply"]:
                    if not (media_dir / photo_name).exists():
                        os.rename(photo.image.path, media_dir / photo_name)
                    photo.image.name = photo_name
                    photo.save()
