# This file is part of photo21
# Copyright (C) 2022  Amicale des élèves de l'ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

import django.core.validators
import django.db.models.deletion
import django.utils.timezone
from django.conf import settings
from django.db import migrations, models

import photologue.models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name="PhotoSize",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "name",
                    models.CharField(
                        help_text='Photo size name should contain only letters, numbers and underscores. Examples: "thumbnail", "display", "small", "main_page_widget".',
                        max_length=40,
                        unique=True,
                        validators=[
                            django.core.validators.RegexValidator(
                                message="Use only plain lowercase letters (ASCII), numbers and underscores.",
                                regex="^[a-z0-9_]+$",
                            )
                        ],
                        verbose_name="name",
                    ),
                ),
                (
                    "width",
                    models.PositiveIntegerField(
                        default=0,
                        help_text='If width is set to "0" the image will be scaled to the supplied height.',
                        verbose_name="width",
                    ),
                ),
                (
                    "height",
                    models.PositiveIntegerField(
                        default=0,
                        help_text='If height is set to "0" the image will be scaled to the supplied width',
                        verbose_name="height",
                    ),
                ),
                (
                    "quality",
                    models.PositiveIntegerField(
                        choices=[
                            (30, "Very Low"),
                            (40, "Low"),
                            (50, "Medium-Low"),
                            (60, "Medium"),
                            (70, "Medium-High"),
                            (80, "High"),
                            (90, "Very High"),
                        ],
                        default=70,
                        help_text="JPEG image quality.",
                        verbose_name="quality",
                    ),
                ),
                (
                    "upscale",
                    models.BooleanField(
                        default=False,
                        help_text="If selected the image will be scaled up if necessary to fit the supplied dimensions. Cropped sizes will be upscaled regardless of this setting.",
                        verbose_name="upscale images?",
                    ),
                ),
                (
                    "crop",
                    models.BooleanField(
                        default=False,
                        help_text="If selected the image will be scaled and cropped to fit the supplied dimensions.",
                        verbose_name="crop to fit?",
                    ),
                ),
                (
                    "pre_cache",
                    models.BooleanField(
                        default=False,
                        help_text="If selected this photo size will be pre-cached as photos are added.",
                        verbose_name="pre-cache?",
                    ),
                ),
                (
                    "increment_count",
                    models.BooleanField(
                        default=False,
                        help_text='If selected the image\'s "view_count" will be incremented when this photo size is displayed.',
                        verbose_name="increment view count?",
                    ),
                ),
            ],
            options={
                "verbose_name": "photo size",
                "verbose_name_plural": "photo sizes",
                "ordering": ["width", "height"],
            },
        ),
        migrations.CreateModel(
            name="Tag",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "name",
                    models.CharField(max_length=250, unique=True, verbose_name="name"),
                ),
                (
                    "slug",
                    models.SlugField(
                        help_text='A "slug" is a unique URL-friendly title for an object.',
                        max_length=250,
                        unique=True,
                        verbose_name="slug",
                    ),
                ),
            ],
            options={
                "verbose_name": "tag",
                "verbose_name_plural": "tags",
                "ordering": ["name"],
            },
        ),
        migrations.CreateModel(
            name="Photo",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "image",
                    models.ImageField(
                        upload_to=photologue.models.get_storage_path,
                        verbose_name="image",
                    ),
                ),
                (
                    "date_taken",
                    models.DateTimeField(
                        blank=True,
                        help_text="Date image was taken; is obtained from the image EXIF data.",
                        null=True,
                        verbose_name="date taken",
                    ),
                ),
                (
                    "view_count",
                    models.PositiveIntegerField(
                        default=0, editable=False, verbose_name="view count"
                    ),
                ),
                (
                    "crop_from",
                    models.CharField(
                        blank=True,
                        choices=[
                            ("top", "Top"),
                            ("right", "Right"),
                            ("bottom", "Bottom"),
                            ("left", "Left"),
                            ("center", "Center (Default)"),
                        ],
                        default="center",
                        max_length=10,
                        verbose_name="crop from",
                    ),
                ),
                (
                    "title",
                    models.CharField(max_length=250, unique=True, verbose_name="title"),
                ),
                (
                    "slug",
                    models.SlugField(
                        help_text='A "slug" is a unique URL-friendly title for an object.',
                        max_length=250,
                        unique=True,
                        verbose_name="slug",
                    ),
                ),
                ("caption", models.TextField(blank=True, verbose_name="caption")),
                (
                    "date_added",
                    models.DateTimeField(
                        default=django.utils.timezone.now, verbose_name="date added"
                    ),
                ),
                (
                    "license",
                    models.CharField(
                        blank=True, max_length=255, verbose_name="license"
                    ),
                ),
                (
                    "is_public",
                    models.BooleanField(
                        default=True,
                        help_text="Public photographs will be displayed in the default views.",
                        verbose_name="is public",
                    ),
                ),
                (
                    "owner",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to=settings.AUTH_USER_MODEL,
                        verbose_name="owner",
                    ),
                ),
            ],
            options={
                "verbose_name": "photo",
                "verbose_name_plural": "photos",
                "ordering": ["-date_added"],
                "get_latest_by": "date_added",
            },
        ),
        migrations.CreateModel(
            name="Gallery",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "date_added",
                    models.DateTimeField(
                        default=django.utils.timezone.now, verbose_name="date published"
                    ),
                ),
                (
                    "title",
                    models.CharField(max_length=250, unique=True, verbose_name="title"),
                ),
                (
                    "slug",
                    models.SlugField(
                        help_text='A "slug" is a unique URL-friendly title for an object.',
                        max_length=250,
                        unique=True,
                        verbose_name="title slug",
                    ),
                ),
                (
                    "date_start",
                    models.DateField(
                        default=django.utils.timezone.now, verbose_name="start date"
                    ),
                ),
                (
                    "date_end",
                    models.DateField(blank=True, null=True, verbose_name="end date"),
                ),
                (
                    "description",
                    models.TextField(blank=True, verbose_name="description"),
                ),
                (
                    "is_public",
                    models.BooleanField(
                        default=True,
                        help_text="Public galleries will be displayed in the default views.",
                        verbose_name="is public",
                    ),
                ),
                (
                    "photos",
                    models.ManyToManyField(
                        blank=True,
                        related_name="galleries",
                        to="photologue.Photo",
                        verbose_name="photos",
                    ),
                ),
                (
                    "tags",
                    models.ManyToManyField(
                        blank=True,
                        related_name="galleries",
                        to="photologue.Tag",
                        verbose_name="tags",
                    ),
                ),
            ],
            options={
                "verbose_name": "gallery",
                "verbose_name_plural": "galleries",
                "ordering": ["-date_added"],
                "get_latest_by": "date_added",
            },
        ),
    ]
