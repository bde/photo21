# This file is part of photo21
# Copyright (C) 2022  Amicale des élèves de l'ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("photologue", "0001_initial"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="gallery",
            options={
                "get_latest_by": "date_start",
                "ordering": ["-date_start"],
                "verbose_name": "gallery",
                "verbose_name_plural": "galleries",
            },
        ),
        migrations.RemoveField(
            model_name="gallery",
            name="date_added",
        ),
    ]
