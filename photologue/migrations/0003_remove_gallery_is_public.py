# This file is part of photo21
# Copyright (C) 2022  Amicale des élèves de l'ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("photologue", "0002_auto_20220130_1020"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="gallery",
            name="is_public",
        ),
    ]
