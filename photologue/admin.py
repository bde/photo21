# This file is part of photo21
# Copyright (C) 2022  Amicale des élèves de l'ENS Paris-Saclay
# SPDX-License-Identifier: GPL-3.0-or-later

from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from .models import Gallery, Photo, Tag


class GalleryAdmin(admin.ModelAdmin):
    list_display = ("title", "date_start", "photo_count", "get_tags")
    list_filter = ["date_start", "tags"]
    date_hierarchy = "date_start"
    prepopulated_fields = {"slug": ("title",)}
    model = Gallery
    exclude = ["photos"]
    autocomplete_fields = ["tags"]
    search_fields = [
        "title",
    ]

    def get_tags(self, obj):
        return ", ".join([t.name for t in obj.tags.all()])

    get_tags.short_description = _("tags")


class PhotoAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "date_taken",
        "date_added",
        "is_public",
        "view_count",
        "admin_thumbnail",
        "get_owner",
    )
    list_filter = ["date_added", "is_public", "owner"]
    search_fields = ["title", "slug", "caption"]
    list_per_page = 25
    prepopulated_fields = {"slug": ("title",)}
    readonly_fields = ("date_taken",)
    model = Photo

    def get_owner(self, obj):
        return obj.owner.username

    get_owner.admin_order_field = "owner"
    get_owner.short_description = _("owner")


class TagAdmin(admin.ModelAdmin):
    list_display = ("name",)
    search_fields = ("name",)
    prepopulated_fields = {"slug": ("name",)}
    model = Tag


admin.site.register(Gallery, GalleryAdmin)
admin.site.register(Photo, PhotoAdmin)
admin.site.register(Tag, TagAdmin)
